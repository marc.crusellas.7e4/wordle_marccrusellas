# Wordle_marcCrusellas

Joc de consola que imita el joc de **_Wordle_**.

## Parts implementades

Tot està implementat en teoria.

## Eines utilitzades

- MutableList \<T>
- Scanner
- IOError

## Eines _externes_ utilitzades

- Carpeta colors, però algunes coses innecessàries eliminades.

## Extres

Hi ha només un extra, pots triar si vols jugar amb vides limitades o il·limitades.

<details><summary>Descripció del Projecte</summary>
Joc que et demana endevinar una paraula de 5 lletres d'una petita selecció.
</details>
<details><summary>Com instal·lar i executar el projecte</summary>
Primer de tot clonar el repositori, després extreure el projecte, obrir-lo amb IntelliJ IDEA i executar.
</details>
<details><summary>Instruccions d'ús del joc</summary>
El primer que et demanda el joc, és, si vols jugar amb vides o sense, després de triar ja pots la teva primera paraula i així fins a endevinar la paraula o si jugues amb vides fins que s'acabi.
</details>

**Llicència X11 (Licencia MIT)**

