import colors.*
import java.io.IOError
import java.util.*


var paraulesBase = mutableListOf(
    "dogma",
    "creua",
    "campi",
    "APINYO",
    "pegat",
    "prenc",
    "pilot",
    "guita",
    "glans",
    "galop",
    "pupil",
    "palau",
    "fusta",
    "finca",
    "watts",
    "quilo",
    "lumen"
)
var paraulesUsuari = mutableListOf<String>()
var paraulaGuanyadora = paraulesBase.random().uppercase(Locale.getDefault())

val scan = Scanner(System.`in`)
var finit = true


/**
 * @author Marc Crusellas Serra
 *
 */
fun main() {

    println("Bon dia, vols jugar amb vides o fins que aconsegueixis passar?")
    println("Amb vides = true, Sense vides = false")
    try {
        val election = scan.nextLine()
        when (election.first()) {
            in "truec" -> finit = true
            in "flase" -> finit = false
            else -> println(":)")
        }
        println("${if (finit) "Amb" else "Sense"} vidas")
    } catch (_: IOError) { }
    partidaConVidas()
}

/**
 * Aquesta és la funció secundària, que manté el joc en marxa
 *
 * Aquesta funció juga amb una variant externa.
 * Que es modifica si és necessari a l'inici.
 * ```
 * var finit = true
 * ```
 *
 * @author Marc Crusellas Serra
 *
 * @see entradaUsuari
 *
 */
fun partidaConVidas() {
    var guanyat = false

    var vides = 6
    dowhile@ do {
        if (vides <= 0) {
            break@dowhile
        }
        entradaUsuari()

        taulell()
        val paulaAComprovar = paraulesUsuari.lastOrNull()
        if (!paulaAComprovar.isNullOrBlank()) {
            guanyat = comprovacio(paulaAComprovar, false) as Boolean
        }
        if (finit) {
            vides--
        }


    } while (!guanyat)
    if (guanyat) {
        println("Ole guanyador")
    } else {
        println("En el dubte de si dir-te alguna cosa, no mereixes aquest pc")
    }
}

/**
 * Fer que l'usuari entri una combinació de 5 lletres
 *
 * @author Marc Crusells Serra
 */
fun entradaUsuari() {


    var b = true
    var paulaUsuari: String
    var intents = 0

    // no surt fins que ho aconsegueix

    while (b) {
        println("Bon dia, una paraula?")

        paulaUsuari = scan.next().uppercase(Locale.getDefault())
        if (paulaUsuari.length == 5) {
            paraulesUsuari.add(paulaUsuari)
            b = false
        }
        intents++
    }
}

/**
 * Encarregat de mostrar el taulell
 *
 * He fet servir el següent codi per evitar que el Cast donés un error, l'error era que no hi havia cap classe de prova de què no dones error.
 * ```
 * @Suppress("UNCHECKED_CAST")
 * ```
 */
@Suppress("UNCHECKED_CAST")
fun taulell() {
    val bueltas = if (finit) 5 else paraulesUsuari.size - 1
    println(" ----------------".blackBackground())

    for (linea in 0..bueltas) {
        val entregarAFuncio = paraulesUsuari.elementAtOrNull(linea)
        print("|".blackBackground())
        if (!entregarAFuncio.isNullOrBlank()) {
            val row: MutableList<String> = comprovacio(entregarAFuncio, true) as MutableList<String>

            row.forEach {
                print(it)
            }
        } else {
            for (rc in 0..4) {
                print(" * ".blackBackground())
            }
        }
        println("|".blackBackground())

    }

    println(" ----------------".blackBackground())
}

/**
 * Funció per a formatar i posar color o mirar si és la mateixa paule.
 * @param paulaUser: String, b: Boolean
 * @param b: Boolean
 *
 * @return Any: per no haver de repetir una funció molt semblant, les dos coses que retorne són:
 * ```
 * MutableList<String>
 * ```
 * y
 * ```
 * Boolean
 * ```
 */
fun comprovacio(paulaUser: String, b: Boolean): Any {
    // b = true és igual a què vols colors paraules per paraula
    return if (b) {
        val mutableList = mutableListOf<String>()

        paulaUser.forEachIndexed { index, c ->
            when (c) {
                paraulaGuanyadora[index] -> mutableList.add(" $c ".greenBackground())
                in paraulaGuanyadora -> mutableList.add(" $c ".yellowBackground())
                else -> mutableList.add(" $c ")
            }
        }
        mutableList
    } else {
        paulaUser == paraulaGuanyadora
    }
}