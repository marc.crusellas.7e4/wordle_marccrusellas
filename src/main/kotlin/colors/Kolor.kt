package colors

object Kolor {
    internal const val ESCAPE = '\u001B'
    private const val RESET = "$ESCAPE[0m"

    fun background(string: String, color: Color) = color(string, color.background)

    private fun color(string: String, ansiString: String) = "$ansiString$string$RESET"
}