package colors

fun String.blackBackground() = Kolor.background(this, Color.BLACK)

fun String.greenBackground() = Kolor.background(this, Color.GREEN)

fun String.yellowBackground() = Kolor.background(this, Color.YELLOW)


