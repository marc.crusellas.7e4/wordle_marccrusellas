package colors

import colors.Kolor.ESCAPE


private const val BG_JUMP = 10


enum class Color(baseCode: Int) {
    BLACK(30),
    GREEN(32),
    YELLOW(33);
    val background: String = "$ESCAPE[${baseCode + BG_JUMP}m"
}